using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinPacking
{
    public class BinCompletionGenerator
    {

        public BinCompletionGenerator()
        {

        }

        /// <summary>
        /// Yields feasible subsets of given items, whose sums are in the given range,
        /// while accounting for domination.
        /// </summary>
        /// <param name="sortedSet">List of elements, sorted in ascending order.</param>
        /// <param name="index">Index of largest element to consider.</param>
        /// <param name="capacity">Bin capacity.</param>
        /// <param name="minSum">Lower bound for subset sums.</param>
        /// <param name="maxSum">Upper bound for subset sums.</param>
        /// <returns>IEnumerable of feasible subsets.</returns>
        public IEnumerable<List<int>> Generate(List<int> sortedSet, int index, int capacity, int minSum)
        {
            if (sortedSet == null || sortedSet.Count <= 0 || index < 0 || index >= sortedSet.Count) yield break;
            List<int> included = new List<int>() { sortedSet[index--] };
            int sumOfRemaining = sortedSet.Sum() - included.Sum();
            int sumOfIncluded = included.Sum();
            foreach (List<int> subset in Generate(sortedSet, index, capacity, minSum, included, sumOfIncluded, sumOfRemaining))
                yield return subset;
        }


        // Perform an inclusion/exclusion binary tree search with pruning,
        // to generate all feasible subsets and return them one by one.
        private IEnumerable<List<int>> Generate(List<int> set, int index, int capacity, int min, List<int> included, int sumIncluded, int sumRemaining)
        {
            // on a leaf node, or when we hit the upper bound, return the currently held subset
            if (index == -1 || sumIncluded == capacity)
            {
                if(!IsDominated(set, included, capacity))
                    yield return included;
                yield break;
            }

            // if the remaining elements and the included elements don't add up to at least the lower bound, terminate this branch,
            // as no subset in this branch can reach a sum in the given range.
            if (sumIncluded + sumRemaining < min) yield break;

            // we only include elements that, when added to the sum of currently included elements don't exceed the upper bound
            // in the inclusion/exclusion binary search tree, we branch left for including the current element.
            int current = set[index];
            if (current + sumIncluded <= capacity)
            {
                List<int> nextIncluded = included.ToList();
                nextIncluded.Add(current);
                foreach (List<int> subset in Generate(set, index - 1, capacity, min, nextIncluded, sumIncluded + current, sumRemaining - current))
                    yield return subset;
            }

            // if the current element would overflow the sum over the upper bound, we exclude it, and no further considerations need to be made with that case,
            // since this element cannot be an element of any feasible subset in the tree levels below
            if (current + sumIncluded > capacity)
                foreach (List<int> subset in Generate(set, index - 1, capacity, min, included, sumIncluded, sumRemaining - current))
                    yield return subset;
            
            // elements that would sum the included ones up to exactly max are not excluded,
            // as that'd creat dominated sets by default, which we can, want and should avoid at this point.
            // if we exclude a number that is wouldn't sum up the included to max,
            // it could dominate the subsets in the tree below it, and excluding it may have been a mistake.
            // There is an undominated subset in the branches below, with this element excluded, if the remaining
            // elements sum up to more than the here excluded number.
            if ((current + sumIncluded < capacity) && (sumRemaining - current > current))
                foreach (List<int> subset in Generate(set, index - 1, capacity, min, included, sumIncluded, sumRemaining - current))
                    yield return subset;
        }


        // Checks if given set of included nodes is dominated by those elements that were excluded,
        // which is the case, if there is any subset of included, with cardinality greater than one,
        // that sums up to exactly one single element of the excluded nodes.
        private bool IsDominated(List<int> set, List<int> included, int capacity)
        {
            SubsetGenerator generator = new SubsetGenerator();
            int r = capacity - included.Max();
            List<int> excluded = set.Where(i => (i <= r)).ToList();
            foreach (int i in included) excluded.Remove(i);

            foreach(List<int> subset in generator.Generate(included, decreasing:false))
            {
                if (subset.Count <= 1) continue;
                int s = subset.Sum();
                foreach (int x in excluded)
                    if (s == x) return true;
            }

            return false;
        }

       
    }
}
